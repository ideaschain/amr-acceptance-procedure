#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include <signal.h>

ros::Subscriber odom_sub;
void odom_callback(const nav_msgs::Odometry::ConstPtr& msg) {
  double px = msg->pose.pose.position.x;
  double py = msg->pose.pose.position.y;

  double linear  = msg->twist.twist.linear.x ;
  double angular = msg->twist.twist.angular.z;

  double oz = msg->pose.pose.orientation.z;
  double ow = msg->pose.pose.orientation.w;
  double radian_ori = atan2(2*(ow*oz), 1-2*(oz*oz));
  double radian;
  if (radian_ori>0) {
    radian = 2*M_PI - radian_ori;
  } else {
    radian = radian_ori*-1;
  }
  double degree = radian * 180/M_PI ;
  ROS_INFO("\n===========================================================\nSeq: [%d]", msg->header.seq);
  std::cout<<"-------------Position----------------"<<std::endl;
  printf("x: %10.5f\n", px);
  printf("y: %10.5f\n", py);
  std::cout<<"-------------Velocity----------------"<<std::endl;
  printf("Linear:  %10.5f\n", linear);
  printf("Angular: %10.5f\n", angular);
  std::cout<<"-------------Orientation-------------"<<std::endl;
  printf("rad_ori:%10.5f\n", radian_ori);
  printf("radian: %10.5f\n", radian);
  printf("degree: %10.5f\n", degree);
  std::cout<<std::endl;
}

void signal_callback_handler(int signum) {
  odom_sub.shutdown();
  // exit(signum);
  // exit(0);
  abort(); //Force kill;
}


int main(int argc, char **argv) {
  ros::init(argc, argv, "odom_mointor");
  ros::NodeHandle n;

  odom_sub = n.subscribe("odom", 1, odom_callback);
  // odom_sub = n.subscribe("agv_car/odom", 1, odom_callback);

  signal(SIGINT, signal_callback_handler);

  ros::spin();
  return 0;
}